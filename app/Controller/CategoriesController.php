<?php

namespace App\Controller;


use App\Helpers\SlugHelper;
use App\Repositories\CategoriesRepository;

/**
 * Controller responsável pelo gerenciamento de categorias de produtos.
 *
 * Class CategoriesController
 * @package App\Controller
 * @author Pedro Lazari <plazari96@gmail.com>
 */
class CategoriesController extends BaseController
{
    /**
     * Responsável por exibir todos os itens das categorias;
     * @author  Pedro Lazari <plazari96@gmail.com>
     */
    public function index()
    {
        $CategoryRepository = new CategoriesRepository();

        $category = $CategoryRepository->all();

        $this->setVariables('message', flash()->display());
        $this->setVariables('categories', $category);
        echo $this->render('categories/categories.php');
    }

    /**
     * Responsável por exibir a tela de criar categoria
     * @author  Pedro Lazari <plazari96@gmail.com>
     */
    public function create()
    {
        $this->setVariables('message', flash()->display());
        $this->setVariables('formRoute', '/categories/store');
        echo $this->render('categories/addCategory.php');
    }

    /**
     * Responsável por gravar a categoria no banco de dados
     * @author  Pedro Lazari <plazari96@gmail.com>
     */
    public function store()
    {
        $CategoryRepository = new CategoriesRepository();

        $category = [
            'name' => $this->post('name'),
            'code' => SlugHelper::toSlug($this->post('code')),
        ];

       $category_id = $CategoryRepository->save($category);


       if ($category_id) {
           \flash()->success('Yeah! YOur category has been created!');
           header('Location: /categories');
       }

    }

    /**
     * Responsável por carregar a categoria e permitir a edição em tela.
     * @param id string ID da categoria no bando de dados
     * @author  Pedro Lazari <plazari96@gmail.com>
     */
    public function edit($id)
    {
        $CategoryRepository = new CategoriesRepository();

        $category = $CategoryRepository->getCategory($id);

        if (!$category) {
            \flash()->error('Oops! Category Not Found!');
            header('Location: /categories');
        }

        $this->setVariables('category', $CategoryRepository->getCategory($id));
        $this->setVariables('formRoute', '/categories/update/'.$id);
        echo $this->render('categories/addCategory.php');
    }

    /**
     * Responsável por gravar a categoria no banco de dados.
     * @param id string ID da categoria no bando de dados
     * @author  Pedro Lazari <plazari96@gmail.com>
     */
    public function update($id)
    {
        $CategoryRepository = new CategoriesRepository();

        $array_update = [
            'id'    => $id,
            'name'  => $this->post('name'),
            'code'  => $this->post('code')
        ];

        $category_id = $CategoryRepository->save(['id' =>  $id], $array_update);

        if ($category_id) {
            \flash()->success('Yeah! Your category has been updated!');
            header('Location: /categories');
        }
    }

    /**
     * Responsável por remover a categoria do banco de dados
     * @param id string ID da categoria no bando de dados
     * @author  Pedro Lazari <plazari96@gmail.com>
     */
    public function delete($id){
        $CategoryRepository = new CategoriesRepository();

        $category = $CategoryRepository->delete($id);

        if ($category) {
            \flash()->success('Yeah! Your category has been deleted!');
            return header('Location: /categories');
        }

        \flash()->error('Oops! Your category has not been deleted!');
        return header('Location: /categories');
    }
}
