<?php

namespace App\Controller;

use App\Traits\InputTrait;
use App\Traits\LoggerTrait;

/**
 * Controller base da aplicação extendendo controller de Log
 *
 * Class BaseController
 * @package App\Controller
 * @author  Pedro LAzari <plazari96@gmail.com>
 */
class BaseController
{
    use LoggerTrait;
    use InputTrait;

    public $view;

    private $variables;

    public $input;


    public function __construct()
    {

        $twig_loader = new \Twig\Loader\FilesystemLoader(__DIR__ . '/../../resource/views');

        $this->view = new \Twig\Environment($twig_loader);

        $this->setVariables('root', (isset($_SERVER['HTTPS']) ? "https" : "http") . '://' . $_SERVER['HTTP_HOST']);
    }

    /**
     * Facilita a aplicação dos templates gerenciados pelo Twig
     *
     * @param string $template Caminho para o template
     * @return string
     */
    public function render(string $template)
    {
        return $this->view->render($template, $this->variables);
    }

    /**
     * Seta variaveis que serão enviadas para o template
     *
     * @param string $key Chave da variavel.
     * @param mixed $value Valor da variavel.
     * @return $this
     */
    public function setVariables(string $key, $value)
    {
        $this->variables[$key] = $value;

        return $this;
    }
}
