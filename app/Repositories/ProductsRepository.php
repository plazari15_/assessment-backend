<?php


namespace App\Repositories;


use App\Traits\LoggerTrait;
use App\Models\Product;

/**
 * Classe ProductsRepository
 * @package App\Repositories
 */
class ProductsRepository
{
    use LoggerTrait;


    /**
     * Busca todos os produtos salvos no banco de dados com suas categorias
     *
     * @return Product[] com Array de categorias
     */
    public function all()
    {
        $this->logInfo("Iniciando a busca por Produtos com Categorias", []);
        return Product::with('categories')->get();
    }

    /**
     * Retorna produto com categoria após busca no banco pelo ID
     *
     * @param $id
     * @return object Produto
     */
    public function get($id)
    {
        $this->logInfo('Buscando Produto  id:'.$id);

        $Product = Product::find($id);

        return $Product;
    }


    /**
     * Salva na base o produto, atualizando ou criando.
     *
     * @param array $validation contem os campos que devem ser usados para verificar
     * existencia do produto na base.
     * @param array $product Contem os campos que devem ser usados para gravar produto,
     * caso vazio, segue com $validation.
     * @return int ID do produto
     */
    public function save(array $validation, array $product = [])
    {
        $this->logCritical("Iniciando Save do Produto");
        $product = empty($product) ? $validation : $product;

        try {
            $this->logInfo("Gravando produto no Banco de Dados");
            $Product = Product::updateOrCreate($validation, $product);
            $Product->categories()->sync($product['categories']);
        } catch (\Exception $e) {
            $this->logCritical("Erro ao gravar produto no Banco de Dados", $e->getMessage());
            return false;
        }

        $this->logInfo("Categoria gravada no banco de dados", $Product->toArray());

        return $Product->id;
    }

    /**
     * Remove produto por ID
     * @param $id do produto a ser deletado.
     *
     * @return mixed
     */
    public function delete($id)
    {
        $this->logInfo("Removendo a categoria {$id}");
        return Product::find($id)->delete();
    }

}
