<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Classe model de Categorias
 *
 * Class Category
 * @package App\Models
 * @author Pedro Lazari <plazari96@gmail.com>
 */
class Category extends Model
{

    protected $fillable = [
        'name',
        'code'
    ];


    /**
     * Relacionamento N:N com a tabela de Produtos
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products(){
        return $this->belongsToMany('App\Models\Product', 'products_categories');
    }
}
