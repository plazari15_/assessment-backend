<?php

/**
 * Arquivo de rotas, contem todas as rotas necessárias
 * @author Pedro Lazari <plazari96@gmail.com>
 */

// Instanciando aplicação de Rotas
$app = System\App::instance();
$app->request = System\Request::instance();
$route = $app->route = System\Route::instance($app->request);

// Declaração de rotas


$route->get('/', 'App\Controller\DashboardController@index');

$route->group('/categories', function (){
    $this->get('/', 'App\Controller\CategoriesController@index');
    $this->get('/create', 'App\Controller\CategoriesController@create');
    $this->post('/store', 'App\Controller\CategoriesController@store');
    $this->get('/edit/{id}', 'App\Controller\CategoriesController@edit');
    $this->post('/update/{id}', 'App\Controller\CategoriesController@update');
    $this->get('/delete/{id}', 'App\Controller\CategoriesController@delete');
});

$route->group('/products', function (){
    $this->get('/', 'App\Controller\ProductsController@index');
    $this->get('/create', 'App\Controller\ProductsController@create');
    $this->post('/store', 'App\Controller\ProductsController@store');
    $this->get('/edit/{id}', 'App\Controller\ProductsController@edit');
    $this->post('/update/{id}', 'App\Controller\ProductsController@store');
    $this->get('/delete/{id}', 'App\Controller\ProductsController@delete');
});

// Fim das rotas
return $route->end();

