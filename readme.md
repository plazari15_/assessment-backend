# Teste WebJump por Pedro Lazari

Este teste foi elaborado utilizando as boas práticas de PHP E PSR-4

Para desenvolvimento, utilizei as tecnologias

1. PHP 7.2
2. MySQL 5.7
3. Composer
4. Git
5. Docker (Ambientum)

Bibliotecas Utilizadas

1. [Illuminate/database](https://packagist.org/packages/illuminate/database)
2. [nezamy/route](https://packagist.org/packages/nezamy/route)
3. [robmorgan/phinx](https://packagist.org/packages/robmorgan/phinx)
4. [monolog/monolog](https://packagist.org/packages/monolog/monolog)
5. [vlucas/phpdotenv](https://packagist.org/packages/vlucas/phpdotenv)
6. [twig/twig](https://packagist.org/packages/twig/twig)
7. [nesbot/carbon](https://packagist.org/packages/nesbot/carbon)
8. [tamtamchik/simple-flash](https://packagist.org/packages/tamtamchik/simple-flash)
9. [splitbrain/php-cli](https://packagist.org/packages/splitbrain/php-cli)
10. [phpunit/phpunit](https://packagist.org/packages/phpunit/phpunit)

Criei uma estrutura MVC que ao meu ver ficou simples e de fácil compreensão.

Para o importador CSV usei um pacote que facilita a criação do processo e deixei simples e intuitivo para que fosse fácil o uso. FIz de um jeito que ele
pudesse buscar em qualquer lugar o arquivo CSV, e implantei uma lógica que tanto no produto como na categoria possam ser criados ou atualizados se já existirem

##Requisitos para instalar o projeto
Você vai precisar do Docker e do Docker Compose para rodar

1 - [Instalar Docker](https://docs.docker.com/compose/install/)

2 - [Instalar Docker Compose](https://docs.docker.com/compose/install/)

# Para rodar o projeto

1- Clone o projeto no BitBucket
```
git clone https://plazari96@bitbucket.org/plazari15_/assessment-backend.git
```

2- Acesse a pasta e execute o docker
```
docker-compose up -d
```

3- Copie o arquivo .env.example para .env e edite as informaçoes como necessário. Se for utilizar o mesmo Docker, nada precisa ser feito.
```
cp .env.example .env
```

4- Execute o composer install
```
docker-compose run web composer install
```

5- Execute as Migrations dentro do Container
```
docker-compose run web ./vendor/bin/phinx migrate
```
6- Execute o Teste Unitário
```
docker-compose run web  ./vendor/bin/phpunit tests/
``` 

### Caso tenham algum problema com instalação 
```
sudo chmod -R 777 .
```

### Para Rodar o importador
Caso necessário, altere o caminho.
```
docker-compose run web php ./app/Commands/FileImporter.php /var/www/app/import.csv 
```

